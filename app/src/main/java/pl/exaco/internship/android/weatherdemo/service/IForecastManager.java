package pl.exaco.internship.android.weatherdemo.service;

import java.util.List;

import pl.exaco.internship.android.weatherdemo.model.CityWeather;

public interface IForecastManager {

    void getForecastForCity(Integer cityId, RequestCallback<List<CityWeather>> callback);
}
